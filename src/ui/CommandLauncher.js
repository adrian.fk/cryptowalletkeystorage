

module.exports = class CommandLauncher {
    static launchCommand(visitorCommand) {
        if (visitorCommand) {
            visitorCommand.execute();
        }
        else {
            throw "Launcher used without command instructions."
        }
    }
}