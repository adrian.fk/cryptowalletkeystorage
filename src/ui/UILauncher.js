const MainMenu = require("./MainMenu/MainMenuUI.js");

const LaunchCommands = new Map();



LaunchCommands.set(
    "MainMenu",
    () => MainMenu.launch()
);

LaunchCommands.set(
    "Default",
    () => LaunchCommands.get("MainMenu")()
);





module.exports = class UILauncher {
    static launch(launchCommandID) {
        if (LaunchCommands.get(launchCommandID)) LaunchCommands.get(launchCommandID)();
        else LaunchCommands.get("Default")();
    }
}