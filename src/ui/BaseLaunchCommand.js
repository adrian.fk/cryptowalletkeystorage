module.exports = class BaseLaunchCommand {

    constructor(destination) {
        this.onTransitionTo = destination;

        return this;
    }

    withOnTransitionTo(destination) {
        this.onTransitionTo = destination;
        return this;
    }

    set OnTransitionTo(destination) {
        this.onTransitionTo = destination;
    }

    execute() {
        if (this.onTransitionTo) this.onTransitionTo();
    }
}
