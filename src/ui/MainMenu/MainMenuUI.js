const BaseLaunchCommand = require("../BaseLaunchCommand.js")
const MenuController = require("./controller/MenuController.js");

module.exports = class MainMenu extends BaseLaunchCommand {

    constructor() {
        super(
            () => this.initiate()
        );
    }

    initiate() {
        this.controller = new MenuController();
        this.controller.init();
    }

    static launch() {
        const mainMenuCommand = new MainMenu();
        mainMenuCommand.execute();
    }
}