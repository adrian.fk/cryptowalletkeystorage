const MenuPresenter = require("./presenter/MenuPresenter.js")
const MenuConfig = require("../../../configuration/MainMenuConfig.js");

module.exports = class MenuController {

    constructor() {
        this.presenter = new MenuPresenter();
        this.menuModel = MenuConfig.getMenu();
    }

    processInput(input) {
        if (this.menuModel.getEntryByKey(input)) {
            this.menuModel.getEntryByKey(input).getEntryExecution()(() => this.init());
        }
        else {
            if (this.menuModel.getEntryByName(input)) {
                this.menuModel.getEntryByName(input).getEntryExecution()(() => this.init());
            }
            else {
                this.presenter.presentWrongInput();
            }
        }
    }

    init() {
        this.presenter.presentMenu(
            this.menuModel,
                input => this.processInput(input)
        );
    }
}