const MenuView = require("./view/MenuView.js")

module.exports = class MenuPresenter {

    constructor() {
        this.view = new MenuView();
    }

    presentMenu(menuModel, inputHandler, wrongInput) {
        this.menuModelCache = menuModel;
        this.inputHandlerCache = inputHandler;

        let menuEntries = menuModel.getAllEntries();

        this.view.clear();
        this.view.displayHeader(menuModel.getTitle(), menuModel.getWidth());
        menuEntries.forEach(
            (menuEntry) => {
                this.view.displayMenuEntry(menuEntry.getEntryKey(), menuEntry.getEntryName(), menuModel.getWidth());
            }
        )
        this.view.displayFooter(menuModel.getWidth());
        if (wrongInput) this.view.displayWrongInput();
        this.view.getInputBuffer(inputHandler);
    }

    presentWrongInput() {
        this.presentMenu(this.menuModelCache, this.inputHandlerCache, true);
    }
}