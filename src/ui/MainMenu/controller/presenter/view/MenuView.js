const InputReceiver = require("../../../../../common/utils/ConsoleInputReceiver.js");

module.exports = class MenuView {

    constructor() {
        this.inputReceiver = InputReceiver.getInstance();
    }

    appendString(string, appendix) {
        return "" + string + appendix;
    }

    appendStringTimes(string, appendix, times) {
        let out = string;
        for (let i = 0; i < times; i++) {
            out = this.appendString(out, appendix);
        }
        string = out;
        return out;
    }

    displaySpacing(numSpacing = 1) {
        while (numSpacing-- !== 0) {
            console.log('\n');
        }
    }

    clear() {
        this.displaySpacing(50);
    }

    printTimesInLine(string, times) {
        let combinedString = "";
        for (let i = 0; i < times; i++) {
            combinedString += string;
        }
        console.log(combinedString);
    }

    displayEmptyLine(width) {
        let displayStr = "";

        displayStr = this.appendString(displayStr, "|");
        displayStr = this.appendStringTimes(displayStr, " ", width);
        displayStr = this.appendString(displayStr, "|");

        console.log(displayStr);
    }

    displayMenuEntry(entryKey, entryName, width) {
        if (entryKey && entryName) {
            const separatorStr = "  -  ";
            let displayStr = "";

            displayStr = this.appendString(displayStr, "|");
            displayStr = this.appendStringTimes(displayStr, " ", 4);
            displayStr = this.appendString(displayStr, entryKey);
            displayStr = this.appendString(displayStr, separatorStr);
            displayStr = this.appendString(displayStr, entryName);
            const numSpacing = 1 + width - displayStr.length;
            displayStr = this.appendStringTimes(displayStr, " ", numSpacing);
            displayStr = this.appendString(displayStr, "|");

            console.log(displayStr);
        }
    }

    displayHeader(headerTitle = "", width = 0) {
        if (headerTitle !== "") {
            const totalPadding = width - headerTitle.length;
            const sidesPadding = totalPadding / 2;
            let displayStr = "";

            this.displaySeparationLine(width);


            displayStr = this.appendString(displayStr, "|");
            displayStr = this.appendStringTimes(displayStr, " ", sidesPadding);
            displayStr = this.appendString(displayStr, headerTitle);
            displayStr = this.appendStringTimes(displayStr, " ", sidesPadding);
            displayStr = this.appendString(displayStr, "|");

            console.log(displayStr);

        }
        this.displaySeparationLine(width);
        this.displayEmptyLine(width);
    }

    displaySeparationLine(width) {
        let separationLineStr = "";
        separationLineStr = this.appendString(separationLineStr, " ");
        separationLineStr = this.appendStringTimes(separationLineStr, "-", width);
        separationLineStr = this.appendString(separationLineStr, " ");

        console.log(separationLineStr);
    }

    displayFooter(width) {
        this.displayEmptyLine(width);
        this.displaySeparationLine(width);
    }

    getInputBuffer(callback) {
        this.inputReceiver.queryInput("\n  :: ", callback);
    }

    displayWrongInput() {
        console.log("\n   ", "\x1b[31m", "Wrong Input..", "\x1b[0m");
    }

}