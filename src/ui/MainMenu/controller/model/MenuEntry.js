module.exports = class MenuEntry {

    constructor(entryKey, entryName, entryExecution) {
        this._entryKey = entryKey;
        this._entryName = entryName;
        this._entryExecution = entryExecution;

        return this;
    }

    withEntryKey(entryKey) {
        this._entryKey = entryKey;
        return this;
    }

    withEntryName(entryName) {
        this._entryName = entryName;
        return this;
    }

    withEntryExecution(executionFunction) {
        this._entryExecution = executionFunction;
        return this;
    }

    sethEntryKey(entryKey) {
        this._entryKey = entryKey;
    }

    setEntryName(entryName) {
        this._entryName = entryName;
    }

    setEntryExecution(executionFunction) {
        this._entryExecution = executionFunction;
    }

    getEntryKey() {
        return this._entryKey;
    }

    getEntryName(entryName) {
        return this._entryName;
    }

    getEntryExecution() {
        return this._entryExecution;
    }

    executeEntryFunction() {
        if (this._entryExecution) this._entryExecution();
    }
}