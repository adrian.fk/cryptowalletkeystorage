const MenuEntry = require("./MenuEntry.js");


const DEFAULT_WIDTH = 50;


module.exports = class Menu {

    constructor() {
        this.headerTitle = "";
        this.width = DEFAULT_WIDTH;
        this.enteriesByKey = new Map();
        this.enteriesByName = new Map();
    }

    getMenu() {
        return this;
    }

    getTitle() {
        return this.headerTitle;
    }

    hasHeaderTitle() {
        return this.headerTitle !== "";
    }

    setTitle(headerTitle) {
        this.headerTitle = headerTitle;
    }

    getWidth() {
        return this.width;
    }

    setWidth(width) {
        this.width = width;
    }

    addMenuEntry(menuEntry) {
        this.enteriesByKey.set(menuEntry.getEntryKey(), menuEntry);
        this.enteriesByName.set(menuEntry.getEntryName(), menuEntry);
    }

    getEntryByKey(entryKey) {
        if (this.enteriesByKey.get(entryKey)) return this.enteriesByKey.get(entryKey);
        return undefined;
    }

    getEntryByName(entryName) {
        if (this.enteriesByName.get(entryName)) return this.enteriesByKey.get(entryName);
        return undefined;
    }

    getAllEntries() {
        return Array.from(this.enteriesByName.values());
    }

    removeEntry(menuEntry) {
        this.enteriesByKey.remove(menuEntry);
        this.enteriesByName.remove(menuEntry);
    }

    createMenuEntry(entryKey, entryName, entryExecutionFunction) {
        return new MenuEntry(entryKey, entryName, entryExecutionFunction);
    }

    getEntriesMapByName() {
        return this.enteriesByName;
    }
}