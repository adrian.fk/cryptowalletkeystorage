const BaseConsoleView = require("../BaseConsoleView.js");

module.exports = class RegisterKeysView extends BaseConsoleView {

    constructor(viewWidth) {
        super();
        this.viewWidth = viewWidth;
    }

    displaySeparationLine() {
        const padding = 4;
        let dStr = "";
        dStr = this.appendStringTimes(dStr, " ", padding);
        dStr = this.appendStringTimes(dStr, "_", this.viewWidth - padding);
        console.log(dStr);
    }

    displayHeader(headerTitle = "") {
        console.log("\t", headerTitle);
        this.displaySeparationLine();
    }

    displayError(errMsg) {
        let displayStr = "\n\tERROR";
        if (errMsg) {
            displayStr = this.appendString(displayStr, ": " + errMsg);
        }
        console.log("\x1b[31m", displayStr, "\x1b[0m")
    }
}