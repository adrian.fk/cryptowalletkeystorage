const RegisterKeysView = require("./RegisterKeysView.js");
const KeyTable = require("./../../domain/model/KeyTable.js");
const KeyList = require("./../../domain/model/KeyList.js");
const PatternMatcher = require("../../common/utils/PatternMatcher.js");
const CONST = require("../../common/constants/UserFileConstants.js");


const DEFAULT_NUM_KEYS = 24;

module.exports = class RegisterKeysController {

    constructor() {
        this.view = new RegisterKeysView(50);
        this.keyTable = KeyTable.getInstance();
        this.keyTraverser = { index: 0 }
        this.keyListR = new KeyList(CONST.DEFAULT_KEYLIST_RIGHT_ID);
        this.keyListL = new KeyList(CONST.DEFAULT_KEYLIST_LEFT_ID);

        this.numKeys = DEFAULT_NUM_KEYS;
    }

    isWord(input) {
        return PatternMatcher.matchAllLetters(input);
    }

    addToKeyList(input) {
        if (this.keyTraverser.index < this.numKeys / 2) {
            //Left
            this.keyListL.addListItem(input);
        }
        else {
            //Right
            this.keyListR.addListItem(input);
        }
    }

    finalizeEntry() {
        this.keyTraverser.index++;
        if (this.keyTraverser.index <= this.numKeys - 1) this.handleKeyEntry();
        else {
            this.keyTable.setLeftList(this.keyListL);
            this.keyTable.setRightList(this.keyListR);
            if (this.processCompletedCallback) this.processCompletedCallback();
        }
    }

    processKeyInput(input) {
        if (this.isWord(input)) {
            this.addToKeyList(input);
            this.finalizeEntry();
        }
        else {
            this.view.displayError("Input must contain letters..");
            this.handleKeyEntry();
        }
    }

    handleKeyEntry() {
        const entryNum = this.keyTraverser.index + 1;
        this.view.queryInput("\n\t" + entryNum + ": ", (input) => this.processKeyInput(input));
    }

    processNumKeysInput(input = "0") {
        return new Promise(
            (resolve, reject) => {
                const intInput = parseInt(input);
                if (input === "d" || (intInput > 0 && intInput < 200)) {
                    if (input !== "d") this.numKeys = intInput;
                    else console.log("Default value chosen.. 24 keys..")
                    resolve();
                }
                else {
                    this.view.displayError("Wrong input.. Input must be a number between 1 to 200");
                    reject();
                }
            }
        );
    }

    handleNumKeys() {
        this.view.queryInput("\n\tHow many keys? : ", (input) => {
            this.processNumKeysInput(input)
                .then(() => this.handleKeyEntry())
                .catch(() => this.handleNumKeys());
        });
    }

    init(callback) {
        this.processCompletedCallback = callback;
        this.view.clear();
        this.view.displayHeader("Register Keys");
        this.handleNumKeys()
    }
}