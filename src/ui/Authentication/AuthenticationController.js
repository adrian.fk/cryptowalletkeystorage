const User = require("./../../domain/model/User.js")
const AuthenticationView = require("./AuthenticationView.js");
const KeyGen = require("./../../infrastructure/KeyGenerator.js");

module.exports = class AuthenticationController {

    constructor() {
        this.user = User.getInstance();
        this.view = new AuthenticationView();
    }

    processInput(input) {
        this.user.setPassword(input);
        this.user.setSecretKey(KeyGen.generate(input));
        if (this.processCompletedCallback) this.processCompletedCallback();
    }

    init(callback, err) {
        this.view.clear();
        if (err) {
            this.view.displayError(err);
        }
        this.view.displaySecretKeyQuery(input => this.processInput(input));
        this.processCompletedCallback = callback;
    }
}