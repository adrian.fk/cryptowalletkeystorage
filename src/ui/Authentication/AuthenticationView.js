const BaseConsoleView = require("./../BaseConsoleView.js");

const AuthenticationView = class AuthenticationView extends BaseConsoleView {

    constructor() {
        super();
    }

    displaySecretKeyQuery(inputHandler = function () {}) {
        const queryMsg = "Secret key: ";
        this.queryInput(queryMsg, inputHandler);
    }

    displayError(errMsg) {
        let displayStr = "ERROR";

        if (errMsg) {
            displayStr = this.appendString(displayStr, ": " + errMsg);
        }

        console.log("\x1b[31m", displayStr, "\x1b[0m");
    }
}

module.exports = AuthenticationView;