const InputReceiver = require("./../common/utils/ConsoleInputReceiver.js");

module.exports = class BaseConsoleView {

    constructor() {
    }

    queryInput(queryMsg = "", inputHandler) {
        const inputReceiver = InputReceiver.getInstance();
        inputReceiver.queryInput(queryMsg, inputHandler);
    }

    displaySpacing(numSpacing = 1) {
        while (numSpacing-- !== 0) {
            console.log('\n');
        }
    }

    clear() {
        this.displaySpacing(50);
    }

    printTimes(string, times) {
        for (let i = 0; i < times; i++) {
            console.log(string);
        }
    }

    clearView() {
        this.printTimes("\n", 50);
    }

    appendString(string, appendix) {
        return "" + string + appendix;
    }

    appendStringTimes(string, appendix, times) {
        let out = string;

        for (let i = 0; i < times; i++) {
            out = this.appendString(out, appendix);
        }

        return out;
    }
}