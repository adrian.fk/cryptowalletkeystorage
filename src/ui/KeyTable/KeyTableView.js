const BaseView = require("../BaseConsoleView.js");

module.exports = class KeyTableView extends BaseView {

    constructor(viewWidth = 50) {
        super();
        this.viewWidth = viewWidth;
        this.headerTitle = "Keys Storage";
    }

    appendString(string, appendix) {
        return "" + string + appendix;
    }

    appendStringTimes(string, appendix, times) {
        let out = string;

        for (let i = 0; i < times; i++) {
            out = this.appendString(out, appendix);
        }

        return out;
    }

    displayEmptyTableLine() {
        let displayStr = "";

        displayStr = this.appendString(displayStr, "|");
        displayStr = this.appendStringTimes(displayStr, " ", this.viewWidth - 1);
        displayStr = this.appendString(displayStr, "|");

        console.log(displayStr);
    }

    displaySeparationLine() {
        let separationLineStr = "";
        separationLineStr = this.appendString(separationLineStr, " ");
        separationLineStr = this.appendStringTimes(separationLineStr, "-", this.viewWidth - 1);
        separationLineStr = this.appendString(separationLineStr, " ");

        console.log(separationLineStr);
    }

    displayTablePair(id1 = 0, word1 = "", id2 = 0, word2 = "") {
        const id1Str = "" + id1;
        const id2Str = "" + id2;

        const padding = 4;

        let displayStr = "";

        displayStr = this.appendString(displayStr, "|");
        displayStr = this.appendStringTimes(displayStr, " ", padding);
        displayStr = this.appendString(displayStr, id1Str);
        if (word1 !== "") displayStr = this.appendString(displayStr, ": " + word1);
        else displayStr = this.appendString(displayStr, "  ");

        //Handle spacing
        const spaceBetween = this.viewWidth - displayStr.length - 2 - id2Str.length - word2.length - padding;
        displayStr = this.appendStringTimes(displayStr, " ", spaceBetween);

        //Append table entry 2
        displayStr = this.appendString(displayStr, id2Str)
        if (word2 !== "") displayStr = this.appendString(displayStr, ": " + word2);
        else displayStr = this.appendString(displayStr, "  ");
        //Append padding
        displayStr = this.appendStringTimes(displayStr, " ", padding);
        displayStr = this.appendString(displayStr, "|");

        //print string
        console.log(displayStr);
    }

    displayHeader() {
        const padding = (this.viewWidth - this.headerTitle.length - 2) / 2;
        let displayStr = "";

        this.displaySeparationLine();

        displayStr = this.appendString(displayStr, "|");
        displayStr = this.appendStringTimes(displayStr, " ", padding);
        displayStr = this.appendString(displayStr, this.headerTitle);
        displayStr = this.appendStringTimes(displayStr, " ", padding);
        displayStr = this.appendString(displayStr, "|");

        console.log(displayStr);

        this.displaySeparationLine();
        this.displayEmptyTableLine();
    }

    displayEnd() {
        const displayStr = "Type 'exit' to exit program or 'back' to return to Main Menu: ";
        console.log(displayStr);
    }

    displayEmptyFooter() {
        console.log("");
        this.displaySeparationLine();
        this.displayEnd();
    }

    displayTableFooter() {
        this.displayEmptyTableLine();
        this.displaySeparationLine();
        this.displayEnd();
    }

    displayEmptyKeyBag() {
        let displayStr = "Ops! It seems like you do not have any registered keys..";

        console.log(displayStr);
    }

    displayError(err) {
        console.log("\x1b[31m", err, "\x1b[0m");
    }
}