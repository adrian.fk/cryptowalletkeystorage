const KeyTable = require("./../../domain/model/KeyTable.js");
const KeyTablePresenter = require("./KeyTablePresenter.js");
const KeyTableStub = require("../../data/Stubs/KeyTableMock.js");


module.exports = class KeyTableController {

    constructor() {
        this.model = KeyTable.getInstance();
        this.presenter = new KeyTablePresenter();
    }

    init(processCompletedCallback, err) {
        //Assuming here that KeyTable model has already been loaded and filled.. alt: some error is handled because
        //no model could be loaded.

        this.presenter.presentKeyTable(this.model, (input) => this.inputHandler(input));
        if (err) this.presenter.presentError(err);
        this.processCompletedCallback = processCompletedCallback;
    }

    inputHandler(input) {
        switch (input) {
            case "exit":
                process.exit(0);
                break;

            case "back":
                if (this.processCompletedCallback) this.processCompletedCallback();
                break;

            default:
                this.init(this.processCompletedCallback, "Input error. Only 'exit' and 'back' allowed.")
                break;
        }
    }

    initMock(callback) {
        const mockObj = new KeyTableStub();
        mockObj.fillKeyTable();

        this.init(callback);
    }
}