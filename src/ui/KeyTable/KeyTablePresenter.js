const KeyTable = require("./../../domain/model/KeyTable.js");
const KeyTableView = require("./KeyTableView.js");

module.exports = class KeyTablePresenter {

    constructor() {
        const viewWidth = 51;
        this.view = new KeyTableView(viewWidth);
    }

    presentKeyTable(keyTable, inputHandler) {
        this.view.clear();
        if (keyTable.getSize() > 0) {
            this.view.displayHeader();
            const keyElements = keyTable.getKeys();
            const elementsDivergeVal = Math.floor(keyTable.getSize() / 2);
            for (let leftIndex = 0, rightIndex = elementsDivergeVal;
                 leftIndex < elementsDivergeVal + 1;
                 leftIndex++, rightIndex++) {

                let leftPos = leftIndex + 1;
                let rightPos = rightIndex + 1;

                let rightElement = keyElements[rightIndex];
                let leftElement = keyElements[leftIndex];

                if (rightIndex >= keyTable.getSize()) {
                    rightElement = ""
                    rightPos = "";
                }

                if (leftIndex >= elementsDivergeVal) {
                    leftElement = "";
                    leftPos = "";
                }
                const keyPair = {left: leftElement, right: rightElement};
                if (!(keyPair.left === "" && keyPair.right === ""))
                    this.view.displayTablePair(leftPos, keyPair.left, rightPos, keyPair.right);
            }
            this.view.displayTableFooter();
            this.view.queryInput("", inputHandler);
        }
        else {
            this.view.displayEmptyKeyBag();
            this.view.displayEmptyFooter();
            this.view.queryInput("", inputHandler);
        }
    }

    presentError(err) {
        this.view.displayError(err);
    }
}