const KeyList = require("./KeyList.js");
const CONST = require("../../common/constants/UserFileConstants.js");


module.exports = class KeyTable {

    constructor() {
        if (!KeyTable.INSTANCE) {
            KeyTable.INSTANCE = this;
            this.leftKeyList = new KeyList(CONST.DEFAULT_KEYLIST_LEFT_ID);
            this.rightKeyList = new KeyList(CONST.DEFAULT_KEYLIST_RIGHT_ID);
        }
        return KeyTable.INSTANCE;
    }

    static getInstance() {
        return new KeyTable();
    }

    addKeyPair(keyLeft, keyRight) {
        if (keyLeft && keyRight) {
            this.leftKeyList.addListItem(keyLeft);
            this.rightKeyList.addListItem(keyRight);
        }
    }

    getKeyPair(index) {
        const leftKey = this.leftKeyList.getListItem(index);
        const rightKey = this.rightKeyList.getListItem(index);
        return {leftKey: leftKey, rightKey: rightKey};
    }

    getKeys() {
        return this.leftKeyList.getListArr().concat(this.rightKeyList.getListArr());
    }

    setRightList(rightKeyList) {
        if (rightKeyList) {
            this.rightKeyList.setId(rightKeyList.id);
            this.rightKeyList.setList(rightKeyList.list)
        }
    }

    setLeftList(leftKeyList) {
        if (leftKeyList) {
            this.leftKeyList.setId(leftKeyList.id);
            this.leftKeyList.setList(leftKeyList.list)
        }
    }

    getLeftKeyList() {
        return this.leftKeyList;
    }

    getRightKeyList() {
        return this.rightKeyList;
    }

    getRightKeyListArr() {
        return this.rightKeyList.getListArr();
    }

    getLeftKeyListArr() {
        return this.leftKeyList.getListArr();
    }

    getSizeOld() {
        if (this.leftKeyList.getLength() === this.rightKeyList.getLength()) {
            return this.leftKeyList.getLength();
        }
    }

    getSize() {
        return this.leftKeyList.getLength() + this.rightKeyList.getLength();
    }
}