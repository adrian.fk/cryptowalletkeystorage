module.exports = class KeyList {

    constructor(id) {
        this.id = id;
        this.list = [];
    }

    addListItem(listItem) {
        if (listItem) this.list.push(listItem);
    }

    getListItem(index = -1) {
        if (index > -1 && index < this.list.length) {
            return this.list[index];
        }
    }

    getListArr() {
        return this.list;
    }

    getLength() {
        return this.list.length;
    }

    getId() {
        return this.id;
    }

    setList(list) {
        this.list = [];
        list.forEach((li) => this.list.push(li))
    }

    setId(id) {
        this.id = id;
    }
}