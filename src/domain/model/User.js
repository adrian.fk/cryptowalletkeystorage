module.exports = class User {

    constructor() {
        if (!User.INSTANCE) {
            User.INSTANCE = this;
        }
        this.dataBankPath = "";
        this.secretKey = "";
        this.password = "";

        return User.INSTANCE;
    }

    static getInstance() {
        return new User();
    }

    setDataBankPath(dataBankPath = "") {
        this.dataBankPath = dataBankPath;
    }

    setSecretKey(sKey = "") {
        this.secretKey = sKey;
    }

    setPassword(pass) {
        this.password = pass;
    }

    isConfigured() {
        return this.password !== "" && this.secretKey !== "";
    }
}