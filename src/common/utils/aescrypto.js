const Crypto = require("crypto-js");

module.exports = class AESCrypto {

    static encrypt(inputString, password) {
        return Crypto.AES.encrypt(inputString, password).toString();
    }

    static decrypt(inputCipherString, password) {
        return Crypto.AES.decrypt(inputCipherString, password).toString(Crypto.enc.Utf8);
    }
}