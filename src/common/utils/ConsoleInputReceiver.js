const rl = require("readline");

module.exports = class InputReceiver {

    constructor() {
        if (!InputReceiver.INSTANCE) {
            InputReceiver.INSTANCE = this;
            this.createInterface();
        }
        return InputReceiver.INSTANCE;
    }

    static getInstance() {
        return new InputReceiver();
    }

    createInterface() {
        if (!this.lineReader) {
            this.lineReader = rl.createInterface({
                input: process.stdin,
                output: process.stdout
            });
            this.running = true;
        }
    }

    resume() {
        if (!this.running) {
            if (this.lineReader) {
                this.lineReader.resume();
                this.running = true;
            }
            else {
                this.createInterface();
            }
        }
    }

    queryInput(queryMsg = "", inputHandler) {
        if (!this.running) {
            this.resume();
        }
        this.lineReader.question(
            `${queryMsg}`,
            inputHandler
        )
    }

    pause() {
        if (this.running) {
            this.lineReader.pause();
            this.running = false;
        }
    }

    close() {
        this.running = false;
        this.lineReader.close();
        this.lineReader = undefined;
    }
}