module.exports = class PatternMatcher {

    static matchAllLetters(input) {
        const letters = /^[A-Za-z]+$/;
        return !!input.match(letters);
    }
}