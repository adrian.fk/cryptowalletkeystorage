const DEFAULT_USER_BIN_PATH = "./src/data/user.bin";
const DEFAULT_USER_BANK = "default";

const DEFAULT_KEYLIST_PATH = "./src/data/user/keyList"
const DEFAULT_KEYLIST_LEFT_ID = "LEFT";
const DEFAULT_KEYLIST_RIGHT_ID = "RIGHT";

module.exports = {
    DEFAULT_USER_BIN_PATH,
    DEFAULT_USER_BANK,
    DEFAULT_KEYLIST_PATH,
    DEFAULT_KEYLIST_LEFT_ID,
    DEFAULT_KEYLIST_RIGHT_ID
};