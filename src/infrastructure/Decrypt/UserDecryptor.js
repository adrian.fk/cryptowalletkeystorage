const User = require("../../domain/model/User.js");
const Crypto = require("../../common/utils/aescrypto.js");
const UserFileLoader = require("./../../infrastructure/Files/UserFileLoader.js");


module.exports = class UserDecryptor {

    static decrypt(inputPassword) {
        return new Promise(
            (resolve) => {
                const user = User.getInstance();

                const encryptedSecretKey = user.secretKey;

                const decryptedSecretKey = Crypto.decrypt(encryptedSecretKey, inputPassword);

                user.setSecretKey(decryptedSecretKey);

                if (resolve) resolve()
            }
        );
    }

    static loadAndDecrypt(inputPassword) {
        return new Promise(
            resolve => {
                UserFileLoader
                    .load()
                    .then(() => UserDecryptor.decrypt(inputPassword))
                    .finally(() => resolve());
            }
        );
    }
}