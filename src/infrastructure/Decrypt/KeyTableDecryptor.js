const KeyTable = require("../../domain/model/KeyTable.js");
const KeyListDecryptor = require("./KeyListDecryptor.js");
const KeyTableSaver = require("../Files/KeyTable/KeyTableSaver.js");
const KeyTableLoader = require("../Files/KeyTable/KeyTableLoader.js");


module.exports = class KeyTableDecryptor {

    static decrypt(inputSecretKey) {
        return new Promise((callback) => {
            const keyTable = KeyTable.getInstance();

            KeyListDecryptor
                .decrypt(inputSecretKey, keyTable.getLeftKeyList())
                .then(
                    () => KeyListDecryptor.decrypt(inputSecretKey, keyTable.getRightKeyList())
                        .finally(() => {if (callback) callback()})
                );
        });
    }

    static decryptAndSave(inputSecretKey) {
        return new Promise((resolve, reject) => {
            KeyTableDecryptor
                .decrypt(inputSecretKey)
                .then(() => KeyTableSaver.save().then(resolve).catch(reject));
        });
    }

    static loadAndDecrypt(inputSecretKey) {
        return new Promise(
            (resolve, reject) => {
                KeyTableLoader
                    .load()
                    .then(() => KeyTableDecryptor.decrypt(inputSecretKey))
                    .then(resolve)
                    .catch(reject);
            }
        );
    }

    static loadAndDecryptThenSave(inputSecretKey) {
        return new Promise(
            (resolve, reject) => {
                KeyTableDecryptor
                    .loadAndDecrypt(inputSecretKey)
                    .then(resolve)
                    .catch(reject);
            }
        );
    }
}