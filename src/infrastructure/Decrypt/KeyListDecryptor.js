const KeyList = require("../../domain/model/KeyList.js");
const Crypt = require("../../common/utils/aescrypto.js");

module.exports = class KeyListDecryptor {

    static decrypt(inputSecretKey, keyList) {
        return new Promise(
            (resolve) =>  {
                let decryptedKeys = [];

                keyList.getListArr().forEach(
                    listItem => {
                        const decKey = Crypt.decrypt(listItem, inputSecretKey);
                        decryptedKeys.push(decKey);
                    }
                )

                keyList.setList(decryptedKeys);

                if (resolve) resolve();
            }
        );
    }
}