const {SHA3} = require("sha3");

module.exports = class KeyGenerator {

    static generate(input = "") {
        const hash = new SHA3(256);

        const pf = Math.random();
        const nonce = Math.floor(pf * 100000);

        hash.update("" + nonce + input);

        return hash.digest('hex');
    }
}