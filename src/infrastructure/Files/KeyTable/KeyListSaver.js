const FileSystem = require("fs");
const CONST = require("../../../common/constants/UserFileConstants.js");



module.exports = class KeyListSaver {
    static save(keyList) {
        return new Promise(((resolve, reject) => {
            if (keyList.getLength()) {
                const buffer = Buffer.from(JSON.stringify(keyList));
                FileSystem.writeFile(CONST.DEFAULT_KEYLIST_PATH + keyList.getId(),
                    buffer,
                    'binary',
                    (err) => {
                        if (err) {
                            console.log(err);
                            reject(err);
                        }
                        else {
                            if (resolve) resolve();
                        }
                    })
            }
        }));
    }
}