const KeyTable = require("../../../domain/model/KeyTable.js");
const KeyListLoader = require("./KeyListLoader.js");

module.exports = class KeyTableLoader {

    static load() {
        return new Promise(callback => {
            const keyTable = KeyTable.getInstance();
            KeyListLoader
                .load(keyTable.getLeftKeyList())
                .then(() => KeyListLoader.load(keyTable.getRightKeyList()))
                .then(callback);
        });
    }
}