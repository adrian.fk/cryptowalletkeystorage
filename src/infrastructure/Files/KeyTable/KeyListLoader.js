const FileSystem = require("fs");
const CONST = require("../../../common/constants/UserFileConstants.js");
const KeyTable = require("../../../domain/model/KeyTable.js");

module.exports = class KeyListLoader {
    static load(keyList) {
        return new Promise(
            (resolve, reject) => {
                let buffer = "";
                FileSystem.readFile(CONST.DEFAULT_KEYLIST_PATH + keyList.getId(),
                    'binary',
                    (err, data) => {
                        if (err) {
                            console.log(err)
                            reject(err);
                        }
                        else {
                            buffer = JSON.parse(data);
                            const keyTable = KeyTable.getInstance();
                            if (keyList.getId() === CONST.DEFAULT_KEYLIST_LEFT_ID) keyTable.setLeftList(buffer);
                            if (keyList.getId() === CONST.DEFAULT_KEYLIST_RIGHT_ID) keyTable.setRightList(buffer);
                            if (resolve) resolve();
                        }
                    });
            }
        );
    }
}