const KeyTable = require("./../../../domain/model/KeyTable.js");
const KeyListSaver = require("./KeyListSaver.js");



module.exports = class KeyTableSaver {
    static save() {
        return new Promise((resolve, reject) => {
            const keyTable = KeyTable.getInstance();
            KeyListSaver.save(
                keyTable.getLeftKeyList()
            ).then(() => KeyListSaver.save(keyTable.getRightKeyList()).then(resolve).catch(reject));
        });
    }
}