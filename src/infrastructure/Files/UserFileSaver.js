const FileSystem = require("fs");
const CONST = require("../../common/constants/UserFileConstants.js");
const User = require("./../../domain/model/User.js");

module.exports = class UserFileSaver {
    static save(user = new User()) {
        return new Promise((resolve, reject) => {
            if (user.secretKey.length/*&& user.isConfigured()*/) {
                const buffer = Buffer.from(JSON.stringify(user));
                FileSystem.writeFile(CONST.DEFAULT_USER_BIN_PATH, buffer, 'binary', (err) => {
                    if (err) {
                        console.log(err);
                        if (reject) reject();
                    }
                    else {
                        if (resolve) resolve();
                    }
                })
            }
            else {
                //Handle user not savable, because not everything is configured
                if (reject) reject();
            }
        });
    }
}