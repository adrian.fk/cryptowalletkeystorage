const FileSystem = require("fs");
const CONST = require("../../common/constants/UserFileConstants.js");
const User = require("./../../domain/model/User.js");

module.exports = class UserFileLoader {
    static load() {
        return new Promise(
            (resolve, reject) => {
                let buffer = "";
                FileSystem.readFile(CONST.DEFAULT_USER_BIN_PATH, 'binary', (err, data) => {
                    if (err) {
                        console.log(err)
                        if (reject) reject();
                    }
                    else {
                        buffer = JSON.parse(data);
                        const user = User.getInstance();
                        user.setDataBankPath(buffer.dataBankPath);
                        user.setPassword(buffer.password);
                        user.setSecretKey(buffer.secretKey);
                        if (resolve) resolve();
                    }
                });
            }
        );
    }
}