const User = require("./../domain/model/User");
const UserFileLoader = require("./Files/UserFileLoader");
const crypto = require("../common/utils/aescrypto.js");
const UserDecrypt = require("./Decrypt/UserDecryptor.js");



module.exports = class UserVerification {

    static verifyUserPass(passwordInput) {
        return new Promise(
            (accept, reject) => {
                UserFileLoader
                    .load()
                    .then(() => {
                            const user = User.getInstance();
                            const userPassBuf = crypto.decrypt(user.password, passwordInput);
                            if (userPassBuf === passwordInput) {
                                UserDecrypt
                                    .decrypt(passwordInput)
                                    .then(() => {
                                        user.setPassword(passwordInput);
                                        if (accept) accept();
                                    })
                            }
                            else {
                                if (reject) reject();
                            }
                        }
                    )
            }
        );
    }
}