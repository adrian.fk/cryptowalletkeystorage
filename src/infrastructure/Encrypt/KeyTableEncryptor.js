const KeyTable = require("../../domain/model/KeyTable.js");
const KeyListEncryptor = require("./KeyListEncryptor.js");
const KeyTableSaver = require("../Files/KeyTable/KeyTableSaver.js");


module.exports = class KeyTableEncryptor {

    static encrypt(inputSecretKey) {
        return new Promise(
            (resolve, reject) => {
                const keyTable = KeyTable.getInstance();

                KeyListEncryptor.enc(inputSecretKey, keyTable.getLeftKeyList())
                    .then(() => KeyListEncryptor.enc(inputSecretKey, keyTable.getRightKeyList()))
                    .finally(() => resolve());
            }
        );
    }

    static encryptAndSave(inputSecretKey) {
        return new Promise(
            resolve => {
                KeyTableEncryptor
                    .encrypt(inputSecretKey)
                    .then(() => KeyTableSaver.save())
                    .finally(() => resolve());
            }
        );
    }
}