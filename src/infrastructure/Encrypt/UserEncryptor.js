const User = require("../../domain/model/User.js");
const UserFileSaver = require("../Files/UserFileSaver.js");
const Crypt = require("../../common/utils/aescrypto.js");


module.exports = class UserEncryptor {

    static encryptAndSave(inputPassword) {
        return new Promise(
            resolve => {
                const user = User.getInstance();
                UserEncryptor
                    .encryptUser(inputPassword)
                    .then(() => UserFileSaver.save(user).finally(() => resolve()));
            }
        );
    }

    static encryptUser(inputPassword) {
        const user = User.getInstance();

        const usrPassword = user.password;
        const usrSecretKey = user.secretKey;

        const encryptedPassword = Crypt.encrypt(usrPassword, inputPassword);
        const encryptedSecretKey = Crypt.encrypt(usrSecretKey, inputPassword);

        user.setPassword(encryptedPassword);
        user.setSecretKey(encryptedSecretKey);

        return new Promise(callback => {if (callback) callback()});
    }
}