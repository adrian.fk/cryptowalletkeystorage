const KeyList = require("../../domain/model/KeyList.js");
const Crypt = require("../../common/utils/aescrypto.js");


module.exports = class KeyListEncryptor {

    static enc(inputSecretKey, keyList) {
        let encryptedKeys = [];

        keyList.getListArr().forEach(
            listItem => {
                const encKey = Crypt.encrypt(listItem, inputSecretKey);
                encryptedKeys.push(encKey);
            }
        )

        keyList.setList(encryptedKeys);

        //if (callback) callback();
        return new Promise(callback => {if (callback) callback();});
    }
}