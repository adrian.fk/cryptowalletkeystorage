const Menu = require('../ui/MainMenu/controller/model/Menu.js');
const User = require("./../domain/model/User.js");
const UserEncryptor = require("./../infrastructure/Encrypt/UserEncryptor.js");
const UserDecrypt = require("./../infrastructure/Decrypt/UserDecryptor.js");
const KeyTableController = require("./../ui/KeyTable/KeyTableController.js");
const RegisterKeysController = require("./../ui/RegisterKeys/RegisterKeysController.js");
const KeyTableEncryptor = require("./../infrastructure/Encrypt/KeyTableEncryptor.js");
const KeyTableDec = require("./../infrastructure/Decrypt/KeyTableDecryptor.js");
const AuthenticationController = require("./../ui/Authentication/AuthenticationController.js");
const UserVerification = require("./../infrastructure/UserVerification.js");





let menu = new Menu();

menu.setTitle("Wallet Key Storage");

menu.addMenuEntry(
    menu.createMenuEntry(
        "1",
        "Register keys",
        function (returnToMainMenu) {
            //Function to start use case
            const user = User.getInstance();
            const authenticationController = new AuthenticationController();
            const registerKeysController = new RegisterKeysController();

            registerKeysController.init(
                () => authenticationController.init(
                    () => {
                        KeyTableEncryptor
                            .encryptAndSave(user.secretKey)
                            .then(() => UserEncryptor.encryptAndSave(user.password).finally(() => returnToMainMenu()));
                    }
                )
            );
        }
    )
)

menu.addMenuEntry(
    menu.createMenuEntry(
        "2",
        "Show key table",
        (returnToMainMenu) => {
            //Function to start use case
            const user = User.getInstance();
            const ktController = new KeyTableController();
            const authenticationController = new AuthenticationController();

            authenticationController.init(
                () => {
                    UserVerification
                        .verifyUserPass(user.password)
                        .then(
                            () => UserDecrypt
                                .loadAndDecrypt(user.password)
                                .then(
                                    () => {
                                        KeyTableDec
                                            .loadAndDecrypt(user.secretKey)
                                            .then(() => ktController.init(() => returnToMainMenu()));
                                    }
                                )
                        )
                        .catch(() => returnToMainMenu());
                }
            );

        }
    )
)

menu.addMenuEntry(
    menu.createMenuEntry(
        "e",
        "exit",
        function () {
            //Function to start use case
            process.exit(1337);
        }
    )
)



module.exports = menu;