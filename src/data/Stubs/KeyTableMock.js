const KeyTable = require("./../../domain/model/KeyTable.js");

module.exports = class KeyTableMock {

    constructor() {
        this.model = KeyTable.getInstance();
    }

    fillKeyTable() {
        //1     13
        this.model.addKeyPair("Rosa", "Gul1");
        //2     14
        this.model.addKeyPair("Rosa2", "Gul2");
        //3     15
        this.model.addKeyPair("Rosa3", "Gul3");
        //4     16
        this.model.addKeyPair("Rosa4", "Gul4");
        //5     17
        this.model.addKeyPair("Rosa5", "Gul5");
        //6     18
        this.model.addKeyPair("Rosa6", "Gul6");
        //7     19
        this.model.addKeyPair("Rosa7", "Gul7");
        //8     20
        this.model.addKeyPair("Rosa8", "Gul8");
        //9     21
        this.model.addKeyPair("Rosa9", "Gul9");
        //10     22
        this.model.addKeyPair("Rosa10", "Gul434");
        //11     23
        this.model.addKeyPair("Rosa11", "Gul11");
        //12     24
        this.model.addKeyPair("Rosa12", "Gul12");
    }

    getKeyTable() {
        return this.model;
    }
}